//usr/bin/gcc -Wall -g $0 -o test && exec ./test

#include <assert.h>
#include <string.h>
#include "../microwav.h"

#define BUFSIZE 1024

int cfg[6][5] = {
    {'u',  8, 1, 44100, 1},
    {'s', 16, 1, 44100, 1},
    {'s', 24, 1, 44100, 1},
    {'s', 32, 1, 44100, 1},
    {'f', 32, 1, 44100, 3},
    {'f', 64, 1, 44100, 3},
};

int main(void) {
    for (int i = 0; i < 6; i++) {
        int  wavsize = 0;
        int  pcmsize = 0;
        int ch, sr, fmt, len;
        char pcm[BUFSIZE];
        char ref[BUFSIZE];
        char out[BUFSIZE];

        // slurp
        {
            char path[255];
            sprintf(path, "wav/%c%ic%is%i.wav", cfg[i][0], cfg[i][1], cfg[i][2], cfg[i][3] / 1000);
            FILE* f = fopen(path, "rb");
            assert(f);
            wavsize = fread(ref, 1, BUFSIZE, f);
            fclose(f);
        }
        // reader
        {
            FILE* f = fmemopen(ref, BUFSIZE, "rb");
            int ok = mwav_read_header(f, &ch, &sr, &fmt, &len);
            assert(ok);
            assert(ch == cfg[i][2]);
            assert(sr == cfg[i][3]);
            assert(fmt == ((cfg[i][4] << 8) | cfg[i][1]));
            assert(len == 64);
            pcmsize = ch * (fmt & 255) / 8 * len;
            fread(pcm, 1, pcmsize, f);
            fclose(f);
        }
        // writer
        {
            FILE* f = fmemopen(out, BUFSIZE, "wb");
            int ok = mwav_write_header(f, ch, sr, fmt, len);
            assert(ok);
            fwrite(pcm, 1, pcmsize, f);
            fclose(f);
            assert(memcmp(ref, out, wavsize) == 0);
        }
    }
    puts("ok");
}
