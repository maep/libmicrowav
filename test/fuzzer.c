//usr/bin/clang -Wall -g -O1 -fsanitize=fuzzer,address $0 -o fuzzer && exec ./fuzzer -max_len=64

// libfuzzer test, requires clang
// start with ./fuzzer.c

#include <stdio.h>

#include "../microwav.h"

int LLVMFuzzerTestOneInput(uint8_t *data, size_t size)
{
    FILE* f = fmemopen(data, size, "r");

    int ch, sr, fmt, len;
    mwav_read_header(f, &ch, &sr, &fmt, &len);

    if (f) {
        fclose(f);
    }

    return 0;
}
