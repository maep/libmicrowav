﻿// a header-only wav implementation
// bsd license
// compiles as c and c++
// no gimmicks, no bullshit

#ifndef MICROWAV_H
#define MICROWAV_H

#include <stdio.h>
#include <stdint.h>

#define MWAV_U8  0x108  // unsigned 8-bit PCM
#define MWAV_I16 0x110  // signed 16-bit PCM
#define MWAV_I24 0x118  // signed 24-bit PCM
#define MWAV_I32 0x120  // signed 32-bit PCM
#define MWAV_F32 0x320  // IEEE-754 32-bit PCM
#define MWAV_F64 0x340  // IEEE-754 64-bit PCM

// internal stuff
#define MWAV_ID(id) (uint32_t)(id[0] | (id[1] << 8) | (id[2] << 16)  | (id[3] << 24))
#define MWAV_SET(ptr, val) do { if (ptr) *(ptr) = (val); } while (0)
#define MWAV_SHR(v, n) ((uint8_t)((v) >> (n)))

static inline uint32_t mwav_read(FILE* f, int size)
{
    uint8_t buf[4] = {0};
    fread(buf, 1, size, f);
    return buf[0] | (buf[1] << 8) | (buf[2] << 16) | (buf[3] << 24);
}

static inline void mwav_write(FILE* f, uint32_t v, int size)
{
    uint8_t buf[4] = {MWAV_SHR(v, 0), MWAV_SHR(v, 8), MWAV_SHR(v, 16), MWAV_SHR(v, 24)};
    fwrite(buf, 1, size, f);
}

// read wav header, 1 on success or 0 on error
static inline int mwav_read_header(FILE* f, int* channels, int* samplerate, int* format, int* size)
{
    int err = !f;
    if (!err) {
        err |= mwav_read(f, 4) != MWAV_ID("RIFF");
        err |= fseek(f, 4, SEEK_CUR);
        err |= mwav_read(f, 4) != MWAV_ID("WAVE");
        err |= mwav_read(f, 4) != MWAV_ID("fmt ");
        uint32_t cksize = mwav_read(f, 4);
        uint32_t fmt = mwav_read(f, 2);
        uint32_t ch = mwav_read(f, 2);
        uint32_t sr = mwav_read(f, 4);
        err |= fseek(f, 6, SEEK_CUR);
        uint32_t bps = mwav_read(f, 2);
        if (fmt == 0xfffe) {
            err |= mwav_read(f, 2) != 22;
            bps = mwav_read(f, 2);
            err |= fseek(f, 4, SEEK_CUR);
            fmt = mwav_read(f, 2);
        }
        err |= fseek(f, 20 + cksize, SEEK_SET) != 0;
        err |= !ch || !sr || !fmt || !bps;
        while (!err && mwav_read(f, 4) != MWAV_ID("data")) {
            cksize = mwav_read(f, 4);
            err |= (!cksize) | fseek(f, cksize, SEEK_CUR);
        }
        uint32_t sz = mwav_read(f, 4);
        if (!err) {
            MWAV_SET(channels, ch);
            MWAV_SET(samplerate, sr);
            MWAV_SET(format, (fmt << 8) | bps);
            MWAV_SET(size, sz / (ch * bps >> 3));
        }
    }
    return !err && !ferror(f);
}

// write wav header, 1 on success or 0 on error
static inline int mwav_write_header(FILE* f, int channels, int samplerate, int format, int size)
{
    int      fmt   = format >> 8;
    int      m     = (format & 255) >> 3;
    int      ext   = (fmt == 1 && m > 2) ? 24 : (fmt != 1) ? 2 : 0;
    int      fact  = (fmt != 1 || m > 2) ? 12 : 0;
    uint32_t asize = m * channels * size;
    int      err   = !f || asize < (uint32_t)size;
    if (!err) {
        mwav_write(f, MWAV_ID("RIFF"), 4);
        mwav_write(f, 36 + ext + fact + asize, 4);
        mwav_write(f, MWAV_ID("WAVE"), 4);
        mwav_write(f, MWAV_ID("fmt "), 4);
        mwav_write(f, 16 + ext, 4);
        mwav_write(f, ext == 24 ? 0xfffe : fmt, 2);
        mwav_write(f, channels, 2);
        mwav_write(f, samplerate, 4);
        mwav_write(f, samplerate * m * channels, 4);
        mwav_write(f, m * channels, 2);
        mwav_write(f, m << 3, 2);
        if (ext) {
            mwav_write(f, ext - 2, 2);
        }
        if (ext == 24) {
            uint8_t guid[14] = {0, 0, 0, 0, 16, 0, 128, 0, 0, 170, 0, 56, 155, 113};
            mwav_write(f, m << 3, 2);
            mwav_write(f, 0, 4);
            mwav_write(f, fmt, 2);
            fwrite(guid, 1, 14, f);
        }
        if (fact) {
            mwav_write(f, MWAV_ID("fact"), 4);
            mwav_write(f, 4, 4);
            mwav_write(f, size, 4);
        }
        mwav_write(f, MWAV_ID("data"), 4);
        mwav_write(f, asize, 4);
    }
    return !err && !ferror(f);
}

#endif
